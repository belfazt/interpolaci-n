#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class interpolacion{
    public:
        interpolacion(){
            //int tam;
            cout << "Dime cuantos valores contiene la tabla: ";
            cin >> this->tama;
            this->equis = new double[this->tama];
            this->ye = new double[this->tama];

            for (int i = 0; i < this->tama; i++){
                cout << "Dame el valor para x" << i << ": ";
                cin >> equis[i];
                cout << "Dame el valor para y" << i << ": ";
                cin >> ye[i];
			}
			this->CERO = 0.0001;
        }

        interpolacion(int tam){
            this->tama = tam;
            this->equis = new double[this->tama];
            this->ye = new double[this->tama];

            for (int i = 0; i < this->tama; i++){
                cout << "Dame el valor para x" << i << ": ";
                cin >> equis[i];
                cout << "Dame el valor para y" << i << ": ";
                cin >> ye[i];
            }
			this->CERO = 0.0001;
        }
		
		interpolacion(int tam, double *x, double *y){
			this->tama = tam;
			this->equis = x;
			this->ye = y;
			this->CERO = 0.0001;
		}

		interpolacion(string archivo){
			//cout << "Leyendo Archivo..." << endl;
			vector<double> x;
			vector<double> y;
			double v;
			ifstream ifs;
			ifs.open(archivo.c_str());
			while (!ifs.eof()){
				ifs >> v;
				x.push_back(v);
				ifs >> v;
				y.push_back(v);
			}
			ifs.close();
			this->tama = x.size();
			this->equis = new double[this->tama];
			this->ye = new double[this->tama];
			for (int i = 0; i < this->tama; i++){
				equis[i] = x.at(i);
				ye[i] = y.at(i);
			}
		}

        void print(){
            cout << "X | Y" << endl;
            for (int i = 0; i< this->tama ; i++){
                cout << this->equis[i] <<" | " << this->ye[i] << endl;
            }
        }

		double Newton(double x){
			return this->Newton(x, this->tama);
		}
		double Newton(double x, int orden){
			cout << "Calculando por el m�todo de Newton para el valor: " << x << " con el orden: " << orden << endl;
			if (orden > this->tama){
				cout << "El orden solicitado excede el orden del polinomio" << endl;
				return -1;
			}
			if (this->hConstanteAll()==(-1)){
				this->lagrange(x);
			}
			double h = this->hConstanteAll();
			bool posible;
			double x0=0; 
			double y0 = 0;
			double k = 0;
			int posx0 = 0;
			for (int i = 0; i < this->tama; i++){
				if (this->equis[i]>x){
					y0 = this->ye[i - 1];
					x0 = this->equis[i - 1];
					posible = true;
					posx0 = i-1;
					break;
				}
				posible = false;
			}
			k = (x - x0) / h;
			double yk=0;
			this->generarCambiosy();
			//this->printCambiosY();
			this->completarCambiosy();
			//cout << this->degree << endl;
			for (int i = 0; i <= orden; i++){
				if (i == 0){
					yk += y0;
				}

				else{
					int factorial = 1;
					for (int j = i; j > 1; j--){
						factorial *= j;
					}
					double aumento = (this->cambiosy[posx0][i] * this->kRecursiva(i, k)) / factorial;
					yk += aumento;
				}
			}
			return yk;
		}

		double kRecursiva(int lim, double k){
			if (lim == 1){
				return k;
			}
			else{
				return (k-(lim-1))*kRecursiva(lim - 1, k);
			}
		}
		double hConstanteAll(){
			double h = this->equis[1] - this->equis[0];
			double oldh;
			//errNorm = fabs(pi2 - pi1)*100.0 / pi2;
			for (int i = 2; i < this->tama; i++){
				oldh = h;
				h = this->equis[i] - this->equis[i - 1];
				if (h - oldh>CERO){
					return -1;
				}
			}
			return h;
		}
		void generarCambiosy(){ //generar lamatriz de Deltas para calcular newton y obtiene el grado de la funci�n
			double **a = new double *[this->tama+1];
			for (int i = 0; i < this->tama; i++){
				a[i] = new double[this->tama+1];
			}
			for (int i = 1; i < this->tama+1; i++){
				for (int j = 0; j < this->tama; j++){
					a[j][i] = 0;
				}
			}
			for (int i = 0; i < this->tama; i++){
				a[i][0] = this->ye[i];
				//cout << this->ye[i] << endl;
			}

			for (int i = 1; i < this->tama; i++){
				for (int j = 0; j < this->tama-i; j++)
				{
					a[j][i] = abs(a[j+1][i-1] - a[j][i-1]);
				}
			}
			int oldh, h;
			bool founddegree = false;
			for (int i = 1; i < this->tama; i++){
				founddegree = false;
				oldh = a[i][0];
				//cout << oldh << endl;
				//cout << "::::" << endl;
				for (int j = 1; j < this->tama - i; j++){
					if (!founddegree){
						if (((a[i][j] - oldh) / a[i][j]) >0.00001){
							this->degree = i;
							founddegree = false;
						}
					}
					else{
						a[i][j] = 0;
					}

					
				}
				if (founddegree){
					break;
				}
			}
			this->cambiosy = a;
		}
		void completarCambiosy(){
			//cout << "OVAH HERE" << endl;
			//cout << this->degree << endl;

			double factorConstante = this->cambiosy[0][this->degree-1];
			//cout << factorConstante << endl;
			for (int i = 0; i < this->tama;i++){
				this->cambiosy[i][this->degree] = factorConstante;
			}

			//cout << "BUENO" << endl;
			for (int i = this->degree-1; i >= 1; i--){
				for (int j = 1; j < this->tama; j++){
					this->cambiosy[j][i] = this->cambiosy[j-1][i] + this->cambiosy[j-1][i+1];
				}
			}
			//this->printCambiosY();
		}
		void printCambiosY(){
			for (int i = 0; i < this->tama; i++)
			{
				for (int j = 1; j < this->tama; j++)
				{
					cout << this->cambiosy[i][j]  << ", ";
				}
				cout << endl;
			}
		}

		double lagrange(double aCalc){			
			cout << "Calculando por m�todo de Lagrange para el valor: " << aCalc << endl;
			double res = 0;
			int j;
			double s;
			for (int i = 0; i<this->tama; i++){
				s = 1;
				j = 0;
				while (j<this->tama)
				{
					if (i != j){
						s = s*(aCalc - this->equis[j]) / (this->equis[i] - this->equis[j]);
					}
					j++;
				}
				res = res + s*this->ye[i];
			}
			/*
			cout << "\n\nEL VALOR DE LA FUNCION f( " << aCalc << ") es : " << res;
			cout << endl << endl << endl;
			*/
			return res;
		}
		
		void usar(){
			cout << "Bienvenido al sistema INTERPOL" << endl;
			int meh = 0;
			double aCalc;
			int ord;
			do{
				cout << "#############################################################################" << endl;
				cout << "1. Interpolaci�n/Extrapolaci�n Exacta" << endl;
				cout << "2. Interpolaci�n/Extrapolaci�n Aproximada" << endl;
				cout << "3. Ver datos" << endl;
				cout << "4. Salir del programa" << endl;	
				cout << endl;
				cin >> meh;
				switch (meh){
				case 1:
					
					cout << "Elige el m�todo que quieres usar: " << endl;
					cout << "1. Newton" << endl << "2. Lagrange" << endl;
					cin >> ord;
					switch (ord){
					case 1:
						cout << "Inserta el valor a interpolar: ";
						cin >> aCalc;
						cout << "Resultado: " << Newton(aCalc) << endl;
						break;
					case 2:
						cout << "Inserta el valor a interpolar: ";
						cin >> aCalc;
						cout << "Resultado: " << lagrange(aCalc) << endl;
						break;
					default:
						cout << "Elige una opci�n v�lida" << endl;
						meh = 5;
						break;
					}
					break;
				case 2:
					cout << "Elige el orden para el cual quieres calcular: ";
					cin >> ord;
					cout << "Elige el valor para el cual quieres calcular: ";
					cin >> aCalc;
					//Insertar metodo de newton
					cout <<"Resultado: " << Newton(aCalc, ord) << endl;
					break;
				case 3:
					this->print();
					break;
				case 4:
					cout << "Saliendo" << endl;
					break;
				default:
					cout << "Elige una opci�n v�lida" << endl;
					meh = 5;
					break;
				}
				cout << endl;
			} while (meh != 4);
		}
		

    private:
        int tama;
        double * equis;
        double * ye;
		double ** cambiosy;
		double CERO;
		int degree;

};
